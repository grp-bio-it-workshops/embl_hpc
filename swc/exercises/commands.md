## Identifying our computer

```
hostname
```

## Our first SLURM job

```
srun hostname
```

## Exploring our example program

```
./hpc_example.py -t 10 -l 100
```

## Running example program on on the cluster

```
srun ./hpc_example.py -t 10 -l 100
```

## Submitting a job to the background

```
sbatch ./hpc_example.py -t 60 -l 100
```

## Viewing running jobs

```
squeue
squeue --Format="JobID:6,UserName:15,State:10,Reason:10,TimeUsed:6,NodeList"
```

Redirecting output

```
sbatch --output=output.txt ./hpc_example.py -t 20 -l 100
```

## Creating a larger list

```
sbatch --output=output.txt ./hpc_example.py -t 30 -l 50000000
```

## Examing default limits

```
scontrol show partition
```

## Requesting Additional Resources

```
sbatch --mem=500 --output=output.txt ./hpc_example.py -t 30 -l 50000000
```

## reserving a LARGE amount of memory

```
sbatch --mem=8000 --output=output.txt ./hpc_example.py -t 30 -l 50000000
```

## Interactive jobs

```
srun --pty bash
```

## Limits apply to interactive jobs too

```
srun --mem=250 --pty bash
./hpc_example.py -t 30 -l 50000000
```

## Job dependencies

```
jid=$(sbatch --parsable batch_job.sh)

sbatch --dependency=afterok:$jid batch_job.sh
```
